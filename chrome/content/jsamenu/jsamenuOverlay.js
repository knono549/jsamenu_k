/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var jsamenuOverlay = {
	_common : new jsamenuCommonClass(),

	handleEvent : function(event) {
		switch (event.type) {
			case "load" : this.onLoad(event); break;
			case "unload" : this.onUnload(event); break;
			case "popupshowing" : this.onContextPopupShowing(event); break;
		}
	},

	onLoad : function(event) {
		event.target.removeEventListener("load", this, false);

		this.applySettings();

		var contextpopup = document.getElementById("contentAreaContextMenu");
		if (contextpopup) {
			contextpopup.addEventListener("popupshowing", this, false);
		}
	},

	onUnload : function(event) {
		event.target.removeEventListener("unload", this, false);

		var contextpopup = document.getElementById("contentAreaContextMenu");
		if (contextpopup) {
			contextpopup.removeEventListener("popupshowing", this, false);
		}
	},

	applySettings : function() {
		var prefName = "config.general.menuHidden";
		var defaultValue = this._common.defaultMenuHidden;
		var hidden = this._common.getBoolPref(prefName, defaultValue);
		var menu = document.getElementById("jsamenu-menu");
		if (menu) {
			if (hidden) {
				menu.setAttribute("hidden", "true");
			} else {
				menu.removeAttribute("hidden");
			}
		}
	},

	onContextPopupShowing : function(event) {
		var contextpopup = event.originalTarget;
		if (contextpopup.id != "contentAreaContextMenu") { return; }

		var prefName = "config.general.contextMenuHidden";
		var defaultValue = this._common.defaultMenuHidden;
		var hidden = this._common.getBoolPref(prefName, defaultValue);

		// 表示しない設定でも、画像やリンク上の場合は表示
		if (hidden &&
			gContextMenu && (gContextMenu.onImage || gContextMenu.onLink)) {
			hidden = false;
		}

		var menu = document.getElementById("JsActionsMenu");
		var menuseparator = document.getElementById('JsActionsSep');

		if (! menu) {
			var node = contextpopup.firstChild;
			while (node) {
				if (node.nodeType == node.ELEMENT_NODE &&
					node.localName == "menu") {
					var label = node.getAttribute("label");
					if (label == "JsActions" || label == "Actions") {
						menu = node; break;
					}
				}
				node = node.nextSibling;
			}
		}

		if (! menuseparator && menu) {
			var node = menu.nextSibling;
			if (node &&
				node.nodeType == node.ELEMENT_NODE &&
				node.localName == "menuseparator") {
				menuseparator = node;
			}
		}

		if (menu && menuseparator) {
			if (hidden) {
				menu.setAttribute("hidden", "true");
				menuseparator.setAttribute("hidden", "true");
			} else {
				menu.removeAttribute("hidden");
				menuseparator.removeAttribute("hidden");
			}
		}
	},

	onCommand : function(event) {
		if (typeof JsActions == 'undefined') { return; }
		JsActions.execMenuScript(event);
	},

	onClick : function(event) {
		if (typeof JsActions == 'undefined') { return; }
		JsActions.clickMiddleButtonEvent(event);
	},

	onPopupHidden : function(event) {
		if (typeof JsActions == 'undefined') { return; }
		JsActions.popupHidden(event);
	},

	onPopupShowing : function(event) {
		if (typeof JsActions == 'undefined') {
			this._common.logStringMessage("JSActions is not installed.");
			return;
		}
		JsActions.popupShowing(event);
	}
};
