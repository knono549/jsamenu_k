/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var jsamenuSettings = {
	_common : new jsamenuCommonClass(),

	onLoad : function() {
		var defaultValue = this._common.defaultMenuHidden;
		this._common.getPrefFromId("config.general.menuHidden", defaultValue);
		this._common.
			getPrefFromId("config.general.contextMenuHidden", defaultValue);
	},

	onAccept : function() {
		this._common.setPrefFromId("config.general.menuHidden");
		this._common.setPrefFromId("config.general.contextMenuHidden");

		var windowMediator =
			Components.classes['@mozilla.org/appshell/window-mediator;1'].
			getService(Components.interfaces.nsIWindowMediator);
		var enumerator = windowMediator.getEnumerator("navigator:browser");
		var win;

		while (enumerator.hasMoreElements()) {
			win = enumerator.getNext();
			win.jsamenuOverlay.applySettings();
		}
	}
};
