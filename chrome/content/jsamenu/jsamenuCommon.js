/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function jsamenuCommonClass() {
	this.pref_root = "extensions.jsamenu.";
	this.msg_prefix = 'JSAMenu: ';

	this.defaultMenuHidden = false;
}

jsamenuCommonClass.prototype = {
	_getPrefBranch : function() {
		return Components.classes["@mozilla.org/preferences-service;1"].
			getService(Components.interfaces.nsIPrefBranch);
	},

	getCharPref : function(prefName, defaultValue) {
		try {
			return this._getPrefBranch().
				getComplexValue(this.pref_root + prefName,
								Components.interfaces.nsISupportsString).data;
		} catch (e) {
		}
		return defaultValue != undefined ? defaultValue : null;
	},

	setCharPref : function(prefName, value) {
		var supportsString =
			Components.classes["@mozilla.org/supports-string;1"].
			createInstance(Components.interfaces.nsISupportsString);
		try {
			supportsString.data = value;
			this._getPrefBranch().
				setComplexValue(this.pref_root + prefName,
								Components.interfaces.nsISupportsString,
								supportsString);
		} catch (e) {
		}
	},

	getIntPref : function(prefName, defaultValue) {
		try {
			return this._getPrefBranch().getIntPref(this.pref_root + prefName);
		} catch (e) {
		}
		return defaultValue != undefined ? defaultValue : null;
	},

	setIntPref : function(prefName, value) {
		try {
			this._getPrefBranch().setIntPref(this.pref_root + prefName, value);
		} catch (e) {
		}
	},

	getBoolPref : function(prefName, defaultValue) {
		try {
			return this._getPrefBranch().
				getBoolPref(this.pref_root + prefName);
		} catch (e) {
		}
		return defaultValue != undefined ? defaultValue : null;
	},

	setBoolPref : function(prefName, value) {
		try {
			this._getPrefBranch().
				setBoolPref(this.pref_root + prefName, value);
		} catch (e) {
		}
	},

	getPrefFromId : function(id, defaultValue) {
		var element = document.getElementById(id);
		if (element) {
			switch (element.localName) {
				case "textbox" :
					element.value = this.getCharPref(id, defaultValue);
					break;
				case "checkbox" :
					element.checked = this.getBoolPref(id, defaultValue);
					break;
				case "menulist" :
					element.selectedIndex = this.getIntPref(id, defaultValue);
					break;
			}
		}
	},

	setPrefFromId : function(id) {
		var element = document.getElementById(id);
		if (element) {
			switch (element.tagName) {
				case "textbox" :
					this.setCharPref(id, element.value);
					break;
				case "checkbox" :
					this.setBoolPref(id, element.checked);
					break;
				case "menulist" :
					this.setIntPref(id, element.selectedIndex);
					break;
			}
		}
	},

	logStringMessage : function(msg) {
		var consoleService =
		  Components.classes['@mozilla.org/consoleservice;1'].
		  getService(Components.interfaces.nsIConsoleService);
		try {
			consoleService.logStringMessage(this.msg_prefix + msg);
		} catch (e) {
		}
	}
};
